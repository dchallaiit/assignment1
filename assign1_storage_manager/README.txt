ASSIGNMENT - 1
======================================================

GROUP - 3 MEMBERS
===========================

1. Alexix Edwards
2. Devadas Challa
3. Kapil Gund

FILES INVOLVED
===========================

1. dberror.c
2. dberror.h
3. makefile
4. README.txt
5. storage_mgr.c
6. storage_mgr.h
7. test_assign1_1.c
8. test_helper.h

HOW TO RUN THE SCRIPT
===========================

1) Go to Project root file using Terminal.

2) Type "make clean" to delete old compiled .o files.

3) Type "make" to compile all project files including "test_assign1_1.c" file 

4) Type "./test_assign1" or "make run_test1" to run "test_assign1_1.c" file.

SOLUTION DESCRIPTION
===========================

1. FILE RELATED FUNCTIONS
===========================

The file-related functions, such as create, open, and close files, are used to manage files.
We can utilize read and write methods for storage manager once the file has been created. 

initStorageManager()
--> The storage manager is initialized with this function. In this procedure, we set the file stream object to NULL. 

createPageFile(...)
--> The file name supplied in the argument is used to build a page file. 
--> To create a file, we utilize the C function fopen(). We utilize the 'w+' mode, which creates a new file and allows us to read and write to it. 
--> If the file could not be created, we return RC FILE NOT FOUND, and if everything went properly, we return RC OK. 

openPageFile(...)
--> To open the file, we use the C function fopen() and the 'r' mode to open it in read-only mode. 
--> We also set the values for curPagePos and totalPageNum in struct FileHandle.
--> We use the linux fstat() C function, which returns various file statistics. Using the fstat() method, we can get the file size in batchs. 
--> If the file could not be opened, we return RC FILE NOT FOUND, and if everything went well, we return RC OK. 

closePageFile(...)
--> The page file pointer was set to NULL. 

destroyPageFile:
--> We check to see if the file is in memory, and if it is, we call the delete() C function to remove it. 


2. READ RELATED FUNCTIONS
==========================

The read functions are used to read data blocks from the page file into the disk (memory). It also effortlessly navigates through the blocks. Fseek(..) and fread(..) are C functions that are utilized. 

readBlock(...)
--> We check to see if the page number is correct. The number of pages on each page should be greater than 0 but less than the total number of pages. 
--> Check to see if the page file pointer is available. 
--> Using the valid file pointer we navigate to the given location using fseek()
--> If fseek() succeeds, the data from the page number specified in the paramter is read and stored in the memPage parameter. 

getBlockPos(...)
--> This function retrieves the current page position from FileHandle's curPagePos and returns it. 

readFirstBlock(...)
--> The readBlock(...) method is called with the pageNum argument set to 0. 

readPreviousBlock(....)
--> We use the pageNum argument to call the readBlock(...) function (current page position - 1) 

readCurrentBlock:
--> We use the pageNum argument to call the readBlock(...) function (current page position) 

readNextBlock:
--> The pageNum argument is (current page position + 1) when we call the readBlock(...) function. 

readLastBlock:
--> The readBlock(...) function is called with the pageNum argument (total number of pages - 1) 


3. WRITE RELATED FUNCTIONS
===========================

The write related functions are used to write blocks of data to the page file from the disk (memory).
fseek(..) and fwrite(..) are C functions that are utilized. 

writeBlock(...)
--> We check to see if the page number is correct. The number of pages on each page should be greater than 0 but less than the total number of pages. 
--> Check to see if the page file pointer is available. 
--> Using the valid file pointer we navigate to the given location using fseek()
--> If fseek() succeeds, we use the fwrite() C function to write the data to the correct position and save it in the memPage parameter. 

writeCurrentBlock(...)
--> The writeBlock(...) function is called with the parameter pageNum = current page position. 

appendEmptyBlock(...) 
--> We make an empty block with the size PAGE SIZE. 
--> The file stream's cursor (pointer) is moved to the last page. 
--> Since we just added a new page, write the empty block data and update the total number of pages by one. 

ensureCapacity(...)
--> Check that the number of pages required exceeds the total number of pages, indicating that extra pages are necessary. 
--> Calculate the amount of pages needed and then add that many empty blocks. 
--> The appendEmptyBlock(...) function is used to add empty blocks. 
