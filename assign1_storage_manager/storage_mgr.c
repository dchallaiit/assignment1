#include <stdio.h>
#include <stdlib.h>
#include "storage_mgr.h"
#include <math.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

FILE *fp;
// Initiate the storage manager

extern void initStorageManager (void)
{
    printf("Storage Manager Created\n");

}
//Creating new file with '\0' initialization.

extern RC createPageFile (char *fileName)
{
    //FILE *fp;
    char *storagestr= (char *) calloc(PAGE_SIZE,sizeof(char));
    fp = fopen(fileName,"w+");
    memset(storagestr,'\0',PAGE_SIZE);//write '\0' to ops string of PAGE_SIZE
    fwrite(storagestr,sizeof(char),PAGE_SIZE,fp);//write ops string to file.
    free(storagestr);//free the pointer for no memory leak.
    fclose(fp);
    RC_message="File creation successfully";
    return RC_OK;
}
//Setting FileHandle structure pointer.

extern RC openPageFile(char *fileName, SM_FileHandle *fHandle)
{
    fp = fopen(fileName, "r+");
    if(fp != NULL) // if file not exist i.e NULL
    {
        // File Name:
        fHandle->fileName = fileName;

        //Cur PAGE Position
        fHandle->curPagePos = 0;

        // Total Number of Page present
        fseek(fp,0,SEEK_END);
        int i = (int) ftell(fp);
        int j = i/ PAGE_SIZE;
        fHandle->totalNumPages = j;

        // mgmtInfo
        fHandle->mgmtInfo =fp;
        fclose(fp);
        RC_message="File Opened and Handler Set Successfully.";
        return RC_OK;
    }
        RC_message="File Not Found.";
        return RC_FILE_NOT_FOUND;

}
//Check if the file is open.
extern RC closePageFile (SM_FileHandle *fHandle)
{
    //check if file exists
   if(fopen(fHandle->fileName,"r+") != NULL) 
   {
        // check if the file handler is initiated 
	    if (fHandle->mgmtInfo == NULL) 
	    {
	        RC_message="File Handler is not initiated.";
            return RC_FILE_HANDLE_NOT_INIT;
	    }
	    else{
            fclose(fHandle->mgmtInfo);
            RC_message="File closed successfully.";
            return RC_OK;
	    }
   }
   else{
        RC_message="File not found.";
        return RC_FILE_NOT_FOUND;
   }
}
//Remove page from disk 
extern RC destroyPageFile (char *fileName)
{
    //check if the file exists and if it is open 
	if(fopen(fileName,"r") != NULL)
	{
		remove(fileName);//remove file from disk.
		RC_message="File removed Successfully.";
		return RC_OK;
	}
    //else throw error
	else
	{
		RC_message="File Not Found";
		return RC_FILE_NOT_FOUND;
	}
}
//read page from disk memory
extern RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    fp = fopen(fHandle->fileName, "r");

    //checking if file handler is initialized 
    if (fHandle == NULL)
    {
        RC_message="file handler not initiated.";
        return RC_FILE_HANDLE_NOT_INIT;
    }

    //check if the file exists
    if(fp == NULL)
    {
        RC_message="File Not Found";
        return RC_FILE_NOT_FOUND;
    }
    if(pageNum > fHandle->totalNumPages-1 || pageNum < 0)
    {
        RC_message="Requested page is not exist.";
        return RC_READ_NON_EXISTING_PAGE; //throw error that page does not exist
    }

    fseek(fp, pageNum*PAGE_SIZE*sizeof(char), SEEK_SET);
    fread(memPage, 1, PAGE_SIZE, fp);

    fHandle->curPagePos = pageNum;

    return RC_OK;
}

//Return the current page position
int getBlockPos (SM_FileHandle *fHandle)
{
    if(fHandle != NULL)//check whether the file handle is initiated or not.
	{
		if((fopen(fHandle->fileName,"r")) != NULL)// check whether file exist of not.
		{
			return fHandle->curPagePos;
		}
		else
		{
			RC_message="File Not Found";
			return RC_FILE_NOT_FOUND; //throw error
		}
	}
	else
	{
		RC_message="file handler not initiated.";
		return RC_FILE_HANDLE_NOT_INIT;
	}
}

//read first block
extern RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return  readBlock(0,fHandle,memPage);
}

//read previous block
extern RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return readBlock(fHandle->curPagePos-1,fHandle,memPage);
}

//Read current block where current handler of file.
extern RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return  readBlock(fHandle->curPagePos,fHandle,memPage);
}

//Read the next block of file
extern RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return  readBlock(fHandle->curPagePos+1,fHandle,memPage);
}

//read last block of file
extern RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return  readBlock(fHandle->totalNumPages-1,fHandle,memPage);
}


//Write at given page position from memory
extern RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    int set_pos = (pageNum)*PAGE_SIZE; //store starting position of pageNum
    
    //Check if file handler is initiated 
    if (fHandle == NULL) 
    {
        return RC_FILE_HANDLE_NOT_INIT;
    }
    fp = fopen(fHandle->fileName, "r+"); 
    if(fp == NULL)
    {
        RC_message="File Not Found";
        return RC_FILE_NOT_FOUND; // Throw error if file not found
    }
    if(pageNum!=0)  // write in file but not in first page.
    {
        fHandle->curPagePos = set_pos;
        writeCurrentBlock(fHandle,memPage);
    }
    else 	//write content to the first page
    {
        fseek(fp,set_pos,SEEK_SET);
        int i;
        for(i=0; i<PAGE_SIZE; i++)
        {
            if(feof(fp)) // check file is ending in between writing
            {
                appendEmptyBlock(fHandle); // append empty block at the end of file
            }
            fputc(memPage[i],fp);// write content to file
        }
        fHandle->curPagePos = ftell(fp);// set current file position to curPagePos
        fclose(fp); //closing filepointer
    }
    RC_message="File written to block Successfully";
    return RC_OK;
}

//  Write at current page from memory
extern RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    //Check if file handler initiated
    if (fHandle == NULL)
    {
        RC_message="file handler not initiated.";
        return RC_FILE_HANDLE_NOT_INIT;
    }

    
    fp = fopen(fHandle->fileName, "r+");
    if(fp == NULL)
    {
        RC_message="File Not Found";
        return RC_FILE_NOT_FOUND; // Throw error if file not found
    }
    //Store current file position
    long int currPosition = fHandle->curPagePos; 

    //Seek current position
    fseek(fp,currPosition,SEEK_SET); 
    fwrite(memPage,1,PAGE_SIZE,fp);//Write memPage to the file

    //Set current file position to curPagePos
    fHandle->curPagePos = ftell(fp); 
    fclose(fp); 
    RC_message="File Write current block Successfully";
    return RC_OK;
}
//Append empty block at the end of file
extern RC appendEmptyBlock (SM_FileHandle *fHandle)
{
	int i;

	if (fHandle == NULL)
		return RC_FILE_HANDLE_NOT_INIT;

	if(fopen(fHandle->fileName,"r") == NULL)
		return RC_FILE_NOT_FOUND;

	fp = fopen(fHandle->fileName,"r+");
	//seek to the end of the file
	fseek(fp, 0, SEEK_END);
	//writes to the empty page
	for(i = 0; i < PAGE_SIZE; i++)
	{
		fwrite("\0",1, 1,fp);
		fseek(fp,0,SEEK_END);
	}

	fHandle->mgmtInfo = fp;
	fHandle->totalNumPages = (ftell(fp)/PAGE_SIZE);
	return RC_OK;
}

//check if new page needed. If needed append new page at the end
extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle)
{
    // check if the file handler is initiated 
    if (fHandle == NULL)
    {
        RC_message="file handler not initiated.";
        return RC_FILE_HANDLE_NOT_INIT;
    }

    //Add emptly pages if numberOfPages > totalNumPages 
    while(numberOfPages > fHandle->totalNumPages) 
        appendEmptyBlock(fHandle);

    fclose(fp);
    return RC_OK;
}


